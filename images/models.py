from django.db import models

class Image(models.Model):
    image = models.ImageField(upload_to='images/', max_length=100)

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'

    def __unicode__(self):
        return str(self.image)

    def __str__(self):
        return str(self.image)