from rest_framework import serializers

from .models import Image

class ImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(max_length=100, allow_empty_file=False, use_url='temp/images/')
    class Meta:
        model = Image
        fields = ('image',)